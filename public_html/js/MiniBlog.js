/* global Backendless */
/* global Handlebars */
/* global moment*/
$(function () {
    var APPLICATION_ID = "00C1ECA3-3AC6-31FD-FF94-8C923AD33D00", 
        SECRET_KEY = "D4376FE0-57FF-8438-FFAA-58226B7AF600",
        VERSION = "v1";
   
    
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
    
    var postsCollection = Backendless.Persistence.of(Posts).find();

    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data    
    };
    
    Handlebars.registerHelper('format', function (time) {
        return moment(time).format("dddd, MMMM Do YYYY ");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);
    
    $('.main-container').html(blogHTML);
}); 

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}

